const form = document.querySelector("#addTodoForm");
const input = document.querySelector("#todoNameInput");
const todoList = document.querySelector("#todoList");

// add submit event listener to form element
// that has an id of #addTodoForm
form.addEventListener("submit", function (event) {
  event.preventDefault();
  // if input value is not 'undefined' or '""' or `null`
  if (input.value) {
    // append todo item with text of input
    // input's text is `input.value`
    appendListItem(input.value);
    // then call reset form
    // to make input value empty
    form.reset();
  }
});

// add click event listener to ul element that has an id of #todoList
// Note: we make use of event delegation (or event bubbling) here
todoList.addEventListener("click", function (event) {
  // if event target - the HTML element that we have clicked
  // - has `.btn-delete` class, then delete todo item
  if (event.target.matches(".btn-delete")) {
    deleteItem(event);
  }

  // if event target - the HTML element that we have clicked
  // - has `.btn-done` class, then mark todo item as done
  if (event.target.matches(".btn-done")) {
    markItemAsDone(event);
  }
});

function deleteItem(event) {
  // event.target is delete button
  // its parent is li element that we want to delete
  const listItem = event.target.parentNode;
  // delete li element from ul element
  todoList.removeChild(listItem);
}

function markItemAsDone(event) {
  // mark as done button is event.target
  // event target is the HTML element that we have clicked
  const markAsDoneButton = event.target;
  // list item is parent of the event.target
  const listItem = event.target.parentNode;
  // If listItem has class 'list-group-item-success'
  // then the item marked as done before
  if (listItem.classList.contains("list-group-item-success")) {
    // set markAsDoneButton text into 'Mark as done'
    markAsDoneButton.innerText = "Mark as done";
  } else {
    // set markAsDoneButton text into 'Mark as undone'
    markAsDoneButton.innerText = "Mark as undone";
  }
  // add green background to li element
  // by adding or removing 'list-group-item-success' class to it
  listItem.classList.toggle("list-group-item-success");
  // add or remove line through span element
  // by adding 'text-decoration-line-through' class to its
  const spanElement = listItem.querySelector("span");
  spanElement.classList.toggle("text-decoration-line-through");
}

function appendListItem(text) {
  // create li element and set its className
  const listItem = document.createElement("li");
  listItem.className =
    "list-group-item d-flex justify-content-between align-items-center";

  // create span element and set its inner text
  // to the text that was written in input
  const spanElement = document.createElement("span");
  spanElement.innerText = text;

  // create button element, set its type to 'button'
  // add classes to it, to make it appear as Bootstrap button
  // and set its text to 'Mark as done'
  const markAsDoneButton = document.createElement("button");
  markAsDoneButton.type = "button";
  markAsDoneButton.className = "btn btn-light btn-done btn-sm ms-auto me-1";
  markAsDoneButton.innerText = "Mark as done";

  // create button element, set its type to 'button'
  // add classes to it, to make it appear as Bootstrap button
  // and set its text to 'Delete'
  const deleteButton = document.createElement("button");
  deleteButton.type = "button";
  deleteButton.className = "btn btn-light btn-delete btn-sm";
  deleteButton.innerText = "Delete";

  // append or add span element into li element
  listItem.appendChild(spanElement);
  // append or add first button element into li element
  listItem.appendChild(markAsDoneButton);
  // append or add second button element into li element
  listItem.appendChild(deleteButton);

  // and finally append li element into ul element
  // that has an id of #todoList
  todoList.appendChild(listItem);
}
